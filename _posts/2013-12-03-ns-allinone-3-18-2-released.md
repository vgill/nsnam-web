---
id: 2585
title: ns-allinone-3.18.2 released
date: 2013-12-03T19:44:07+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2585
permalink: /news/ns-allinone-3-18-2-released/
categories:
  - News
  - ns-3 Releases
---
An update to the ns-3.18 release, numbered ns-allinone-3.18.2, has now been posted at <https://www.nsnam.org/release/ns-allinone-3.18.2.tar.bz2>.

This contains a single fix to the ns-allinone-3.18.1 release for the Direct Code Execution build system, to fix a build regression.

ns-3.19 is scheduled for mid-December.