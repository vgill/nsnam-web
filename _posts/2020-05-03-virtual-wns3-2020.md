---
layout: post
title:  "ns-3 annual meeting (June 17-18) will be a virtual meeting"
date:   2020-05-03 21:30:00 -0700
categories: events
---
The ns-3 annual meeting, including the [Workshop on ns-3](/research/wns3/wns3-2020/), has been rescheduled to a virtual meeting on the originally scheduled dates of June 17-18, 2020.  The meeting will start at 13:00 UTC on each day, and attendance will be free.  More details on how to register and connect will be published at a later date.
