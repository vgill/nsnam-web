---
id: 1419
title: MILCOM 2011 session on ns-3
date: 2011-09-23T03:38:20+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1419
permalink: /events/milcom-2011-session-on-ns-3/
categories:
  - Events
---
A Thursday, November 10 session at [MILCOM 2011](http://www.milcom.org/) will feature the use of ns-3 in the context of military network research. In this session, Tom Henderson will first provide an overview and brief tutorial of ns-3, followed by a series of presentations. The schedule includes Justin Yackoski (Intelligent Automation, Inc.), who will discuss an interoperability architecture to allow automatic conversion and sharing of radio models among network simulators, Kenneth Renard (ARL), who plans to provide an overview of Mobile Network Modeling Institute (MNMI) activities and their experiences with scalable simulations, and Jeff Ahrenholz (Boeing), who will outline how the [CORE network emulator](http://cs.itd.nrl.navy.mil/work/core/) is being integrated with ns-3.