---
id: 2855
title: ns-3 SOCIS 2014 student announced
date: 2014-05-29T20:59:11+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2855
permalink: /news/ns-3-socis-2014-student-announced/
categories:
  - Events
  - News
---
Natale Patriciello has been selected to participate in the 2014 [European Space Agency Summer of Code in Space (SOCIS)](http://sophia.estec.esa.int/socis2014/). Natale&#8217;s project will focus on TCP improvements for satellite and space environments. This is the ns-3 project&#8217;s second year of participation in SOCIS.