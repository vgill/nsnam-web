---
id: 2875
title: ns-3.20 released
date: 2014-06-17T17:45:20+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2875
permalink: /news/ns-3-20-released/
categories:
  - News
  - ns-3 Releases
---
ns-3.20 was released on 17 June 2014. This release features a new LR-WPAN model providing initial support for IEEE 802.15.4 networks, and integration with the previously released SixLowPan (6LoWPAN) module. A new IPv6 routing protocol (RIPng) model has been added, and support within the Flow Monitor for IPv6 has been added. A new LTE MAC downlink scheduling algorithm named Channel and QoS Aware (CQA) Scheduler is provided by the new CqaFfMacScheduler object. In addition, the [RELEASE_NOTES](http://code.nsnam.org/ns-3.20/file/5f2f0408cdc0/RELEASE_NOTES) list the many bugs fixed and small improvements made.

The next ns-3 release is scheduled for August 2014.