---
id: 2899
title: Completion of 2014 GSoC projects
date: 2014-08-22T14:32:13+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2899
permalink: /news/completion-of-2014-gsoc-projects/
categories:
  - News
---
Four student projects were successfully completed for the 2014 [Google Summer of Code program](http://www.nsnam.org/wiki/GSOC2014AcceptedProjects). The projects accomplished the following:

  * Piotr Gawłowicz extended the ns-3 LTE module to support Fractional Frequency Reuse (FFR). The project consisted of the design and implementation of an API for the development of FFR algorithms and the implementation of a series of state-of-the-art FFR algorithms described in the scientific literature. In addition to the FFR model code, Piotr delivered extensive corresponding test code, documentation and examples. Furthermore, as a by-product of the project, Piotr fixed some LTE module bugs, and developed several additional features such as DL and UL power control, per-Resource Block Radio Environment Maps, a refined CQI model and a refactoring of the power and interference calculation code. The code is scheduled for merge in the upcoming ns-3.21 release which is due in September 2014.
  * Rubén Martínez authored a model of the Licklider Transport Protocol (LTP) from scratch, for inclusion in a future &#8220;Delay-Tolerant Networking (DTN)&#8221; module (also planned to include the Bundle Protocol implementation from Dizhi Zhou&#8217;s SOCIS 2013 project). This model includes about 5000 lines of new code, and is being tested for interoperability against the Trinity College Dublin implementation of LTP by using the ns-3 emulation mode. DTN is an area of active protocol research in the [IRTF](https://irtf.org/dtnrg).
  * Anh Nguyen&#8217;s project focused on comment resolution and testing of the CoDel queue model originally proposed by Andrew McGregor and Dave Taht. CoDel is one of a set of active queue management (AQM) techniques being deployed to solve the [bufferbloat](https://www.bufferbloat.net/) problem in the Internet. CoDel is planned for inclusion in the next ns-3 release, and subsequent variations that build on this model (FQ and SFQ CoDel) are planned for later releases once some issues related to IP header access are resolved.
  * Krishna Teja developed the Multicast Listener Discovery Version 2 (MLDv2) functionality for IPv6. The code is completely new, and closely matches RFC 3810. MLDv2 is included in the internet module, and (when merged) will be automatically enabled for any IPv6 node. Thanks to MLDv2, each router is made aware of the multicast groups each host is interested in, and can dynamically reconfigure its routing table. The protocol is part of an ongoing effort to enhance the multicast routing support for IPv6. PIM-SM is being considered for future implementation.

Detailed project plans and descriptions are linked from the [project wiki page](http://www.nsnam.org/wiki/GSOC2014AcceptedProjects).

Google has posted [an entry about the projects](http://google-opensource.blogspot.com/2014/11/google-summer-of-code-wrap-up-ns-3.html) on their open source blog.