---
id: 1937
title: ns-3.16 released
date: 2012-12-22T03:10:32+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1937
permalink: /news/ns-3-16-released/
categories:
  - Events
  - News
  - ns-3 Releases
---
The ns-3.16 release has now been posted at <http://www.nsnam.org/release/ns-allinone-3.16.tar.bz2>

This release adds support for the [BRITE topology generator](http://www.cs.bu.edu/brite/), and adds eight new MAC scheduler models to the LTE module, based on Dizhi Zhou&#8217;s [Google Summer of Code project](http://www.nsnam.org/wiki/index.php/GSOC2012LTEScheduling).