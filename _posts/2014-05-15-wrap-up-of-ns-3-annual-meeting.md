---
id: 2843
title: Wrap-up of ns-3 annual meeting
date: 2014-05-15T17:16:39+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2843
permalink: /news/wrap-up-of-ns-3-annual-meeting/
categories:
  - Events
  - News
---
The project held its annual meeting from May 5-9, 2014, hosted by Georgia Tech (Atlanta GA), and attended by roughly thirty people. The first two days were devoted to ns-3 training organized by the [ns-3 Consortium](http://www.nsnam.org/overview/ns-3-consortium/), covering an overview of the simulator, with specific focus on ns-3 tracing, WiFi and LTE models, distributed simulations, emulation, and Direct Code Execution. The 6th annual [Workshop on ns-3](http://www.nsnam.org/overview/wns3/wns3-2014/) was held on 7 May, featuring eight paper presentations, five posters, and a keynote talk. On Thursday May 8, the annual meeting of the ns-3 Consortium was held (minutes posted [here](http://www.nsnam.org/docs/consortium/meetings/annual-meeting-notes-2014.pdf)), followed by one and a half days of developer discussions (minutes posted [here](http://www.nsnam.org/docs/meetings/ns-3-developer-meeting-notes-May14.pdf)).