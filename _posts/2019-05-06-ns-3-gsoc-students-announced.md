---
title: ns-3 GSoC students announced
date: 2019-05-06T18:00:00+00:00
author: tomh
layout: post
permalink: /news/ns-3-gsoc-students-announced-2019/
categories:
  - News
---
We are pleased to announce that four students will join the ns-3 project for the 2019 Google Summer of Code!  Congratulations to those students selected, and thank you to the selection committee and especially to our volunteer mentors who will be overseeing the projects.

The selected students and projects are:

 * Tommaso Zugno, Integration of the 3GPP TR 38.901 channel model in the ns-3 spectrum module
 * Liangcheng Yu, Framework of Studying Flow Completion Time Minimization for Data Center Networks in ns-3
 * Mishal Shah, Improving the ns-3 AppStore and linking with bake
 * Apoorva Bhargava, TCP Testing and Alignment 
