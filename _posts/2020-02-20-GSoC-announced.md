---
layout: post
title:  "ns-3 selected for Google Summer of Code"
date:   2020-02-20 11:00:00 -0800
categories: news
excerpt_separator: <!--more-->
---
We are pleased to again be selected for Google Summer of Code, our 11th year in the program.  Student applications are due by March 31.  More information about ns-3 and GSoC can be found [on our wiki page](https://www.nsnam.org/wiki/GSOC2020StudentGuide). 
