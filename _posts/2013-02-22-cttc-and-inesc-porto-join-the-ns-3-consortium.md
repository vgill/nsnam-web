---
id: 2190
title: CTTC and INESC Porto join the NS-3 Consortium
date: 2013-02-22T07:15:42+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2190
permalink: /news/cttc-and-inesc-porto-join-the-ns-3-consortium/
categories:
  - News
---
We&#8217;re pleased to announce two new Executive Members of the NS-3 Consortium: the [Centre Tecnològic de Telecomunicacions de Catalunya (CTTC)](http://www.cttc.es/index.jsp) and the [INstituto de Engenharia de Sistemas e Computadores do Porto (INESC Porto)](http://www.inescporto.pt). Both institutions have a strong history of supporting ns-3 over several years. CTTC, in partnership with Ubiquisys, has led the development of the ns-3 Long Term Evolution (LTE) simulation model, and the Telecommunications and Multimedia Unit at INESC initially funded Gustavo Carneiro&#8217;s ns-3 development activities, and continues to perform wireless research involving ns-3. The Steering Committee will expand to include Dr. Nicola Baldo (CTTC) and Prof. Manuel Ricardo (INESC).