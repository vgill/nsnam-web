---
layout: post
title:  "ns-3 book in Chinese released"
date:   2019-01-03 12:00:00 -0700
categories: news
excerpt_separator: <!--more-->
---
Longtime ns-3 contributor **Dizhi Zhou** has authored a new ns-3 book in Chinese; the English name of the book is "Open Source Network Simulator ns-3 - Architect and Practice". 
<!--more-->
Following a quick start overview of writing and running ns-3 simulations, this book covers ns-3 from the ground up, describing the enhancements to and design patterns of the C++ language used by ns-3, including memory management, management of default configuration values in the simulator, and a dynamic type system. Upon this basis, the models for the various protocol layers in ns-3, starting from the application layer and ending at the wireless layers, are outlined. In the remaining chapters, the reader is introduced to a framework for data collection and statistics, extensions to allow ns-3 simulations to interact with the outside world, ns-3 packet structure and a sampling of related projects.  The book can be purchased at [JD](https://tinyurl.com/yafb5ceu) and [Taobao](https://tinyurl.com/y7rz992p).
