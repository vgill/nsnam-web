---
id: 2040
title: WNS3 2013 accepted papers announced
date: 2013-01-21T00:51:05+00:00
author: WNS3 Chairs
layout: post
guid: http://www.nsnam.org/?p=2040
permalink: /events/wns3-2013-accepted-papers-announced/
categories:
  - Events
tags:
  - simutools
  - wns3
---
[List of accepted papers](http://www.nsnam.org/wns3/wns3-2013/accepted-papers/ "Accepted Papers") for [WNS3 2013](http://www.nsnam.org/wns3/wns3-2013/ "WNS3 2013") has been posted on the website.

&nbsp;

&#8212;

Vedran Miletić and Tommaso Pecorella