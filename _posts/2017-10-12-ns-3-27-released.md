---
id: 3781
title: ns-3.27 released
date: 2017-10-12T16:22:23+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3781
permalink: /news/ns-3-27-released/
categories:
  - News
  - ns-3 Releases
---
ns-3.27 was released on 12 October 2017 and features the following significant changes. 

  * **Carrier Aggregation** support was added to the LTE module.
  * Native TCP models have been extended to include **SACK** and **LEDBAT** models.
  * The Wi-Fi module was extended for partial **802.11ax High Efficiency (HE)** support, and a new **Robust Rate Adaptation Algorithm (RRPAA) rate control** has been added.
  * The traffic-control module has been reworked, with new support for gathering **detailed statistics** about the operations of a queue disc, a new **multi-queue aware queue disc** modelled after the mq qdisc in Linux, and the ability to explicitly trace **queue sojourn time**.

Finally, the release includes numerous bug fixes and small improvements, listed in the <a href="http://code.nsnam.org/ns-3.27/raw-file/a6d4461e1a3d/RELEASE_NOTES" target="_blank">RELEASE_NOTES</a>.