---
title: Issues
layout: page
permalink: /develop/tools/issues/
---
Users who wish to report an issue with the code in any of the project repositories should create a GitLab.com account for themselves and enter a [new issue](https://gitlab.com/nsnam/ns-3-dev/issues/).  Please review the existing issues to avoid entering a duplicate.  Information about this tool is found in [GitLab.com documentation](https://docs.gitlab.com/ee/user/project/issues/).

# Legacy bug tracker (Bugzilla)

The legacy ns-3 bug tracker is a [Bugzilla](https://www.bugzilla.org/) instance at [this location](/bugzilla). New bugs should only be open on GitLab.com.  Instructions on how to report bugs are detailed [elsewhere](/support/report-a-bug). 

