---
title: ns-3.21
layout: page
permalink: /releases/ns-3-21/
---
<div>
  <p>
    ns-3.21 was released on 17 September 2014. Several modules have been extended in this release. The LTE module features support for Frequency Reuse algorithms, and also support for the transport of the S1-U, X2-U and X2-C interfaces over emulated links in real-time emulation mode. The Internet module includes new support for a CoDel queue model and support for TCP Timestamps and Window Scale options. Improvements to the energy modeling of Wifi (sleep mode) and a general energy harvesting model were added. In the network module, a new Packet Socket application and helper were added, and the SimpleNetDevice and channel models extended, to facilitate traffic generation and protocol testing without a dependence on the Internet module. In addition, the <a href="http://code.nsnam.org/ns-3.21/file/9a397051f10a/RELEASE_NOTES">RELEASE_NOTES</a> list the many bugs fixed and small improvements made.
  </p>

  <ul>
    <li>
      The released ns-3.21 source code can be downloaded from <a href="https://www.nsnam.org/release/ns-allinone-3.21.tar.bz2">here</a>
    </li>
    <li>
      The documentation is available in several formats from <a href="/releases/ns-3-21/documentation">here</a>.
    </li>
    <li>
      Errata containing late-breaking information about the release can be found <a href="http://www.nsnam.org/wiki/Errata">here</a>
    </li>
    <li>
      A patch to upgrade from the last release (ns-3.20) can be found <a href="https://www.nsnam.org/release/patches/ns-3.20-to-ns-3.21.patch">here</a>
    </li>
  </ul>
</div>
