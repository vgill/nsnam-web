---
title: Running scripts
layout: page
permalink: /support/faq/running-scripts/
---
<div id="toc_container" class="toc_wrap_right no_bullets">
  <ul class="toc_list">
    <li>
      <a href="#Running_C_scripts"><span class="toc_number toc_depth_1">1</span> Running C++ scripts</a>
    </li>
    <li>
      <a href="#Running_python_scripts"><span class="toc_number toc_depth_1">2</span> Running python scripts</a>
    </li>
    <li>
      <a href="#Running_scripts_in_a_shell"><span class="toc_number toc_depth_1">3</span> Running scripts in a shell</a>
    </li>
    <li>
      <a href="#Running_C_scripts_with_gdb"><span class="toc_number toc_depth_1">4</span> Running C++ scripts with gdb</a>
    </li>
  </ul>
</div>

# <span id="Running_C_scripts">Running C++ scripts</span>

Once the build is done and all tests pass, one generally wants to run a couple of examples as per the [tutorial](/documentation/latest/). The easiest way to do this is to run:

<pre>./waf --run progname
</pre>

To find out the list of available programs, you can do:

<pre>./waf --run non-existent-program-name.
</pre>

# <span id="Running_python_scripts">Running python scripts</span>

The method to run a python script is fairly similar to that of a C++ script except that you need to specify a complete path to the script file, and you need to use the &#8211;pyrun command instead of &#8211;run. For example:

<pre>./waf --pyrun examples/wireless/mixed-wireless.py
</pre>

# <span id="Running_scripts_in_a_shell">Running scripts in a shell</span>

Another way to run ns-3 programs that does not require using the _./waf &#8211;run_ command is to use the ns-3 shell which takes care of setting up all the environment variables necessary to do so:

<pre>./waf shell
</pre>

And, then:

<pre>./build/debug/examples/csma-broadcast
</pre>

# <span id="Running_C_scripts_with_gdb">Running C++ scripts with gdb</span>

If you do not use the ns-3 shell, and if you want to run your simulation under a special tool (valgrind or gdb), you need to use a command template:

<pre>./waf --run csma-cd-one-subnet --command-template="gdb %s"
</pre>

or:

<pre>./waf --run csma-cd-one-subnet --command-template="valgrind %s"
</pre>

&nbsp;
