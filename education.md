---
layout: page
title: Education
permalink: /education/
---

# Using ns-3 in Education

This page is a resource for learning about ns-3 as an educational tool for networking education.

## Papers

The [2009 First International Workshop on Education Technology and Computer Science (ETCS '09)](https://www.computer.org/csdl/proceedings/etcs/2009/3557/01/3557ztoc.pdf) had a paper on teaching ns-3 titled [Research of Teaching on Network Course Based on NS-3](http://ieeexplore.ieee.org/abstract/document/4959116/).

The [2011 Sigcomm Education workshop](http://education.sigcomm.org/Workshop2011/Workshop2011) had a paper regarding ns-3 use in the classroom [An Open-source and Declarative Approach Towards Teaching Large-scale Networked Systems Programming](http://edusigcomm.info.ucl.ac.be/Workshop2011/20110310001).

## Courses using ns-3

The following courses have used ns-3 as courseware or to support student projects

* [IIIT Delhi Wireless Networks CSE/ECE 538](http://faculty.iiitd.ac.in/~mukulika/wn.html).
This course covers a variety of mobile systems (wireless LANs, cellular systems), design of various layers in the network stack in the context of wireless communication. ns-3 has been used for student projects in Monsoon 2017, Winter 2019, and Winter 2020.

*   [Northeastern University EECE 5155](https://wl11gp.neu.edu/udcprod8/bwckctlg.p_disp_course_detail?cat_term_in=201830&subj_code_in=EECE&crse_numb_in=5155) Prof. Tommaso Melodia reported that students used ns-3 in this course to study the performance of a multi-hop IEEE 802.11 network using UDP as transport-layer protocol as different transmission mechanisms, routing algorithms, and network sizes affected performance metrics such as throughput, delay, delivery ratio, and energy consumption.  Spring 2019.
*   [University of Washington EE 595](https://depts.washington.edu/funlab/resources/ee-595-wireless-networks-for-4g5g/), Sumit Roy and Tom Henderson, Winter 2019.
*   [Georgia Tech. ECE 6110](https://griley.ece.gatech.edu/riley/ece6110/) Dr. George Riley, Spring 2013, Fall 2011, and Fall 2010
*   The University of Kansas
    *   [EECS 780](https://www.ittc.ku.edu/~jpgs/courses/nets/) Dr. James Sterbenz, Fall 2017, Fall 2016, Fall 2015, Fall 2014, Spring 2013, Spring 2012, Spring 2011, Spring 2010, Spring 2009
    *   [EECS 882](https://www.ittc.ku.edu/~jpgs/courses/mwnets/) Dr. James Sterbenz, Spring 2016, Fall 2013, Fall 2011, Fall 2009
    *   [EECS 983](https://www.ittc.ku.edu/~jpgs/courses/rsnets/) Dr. James Sterbenz, Spring 2014, Spring 2012, Spring 2010
*   [University of Pennsylvania CIS 553/TCOM 512](https://www.cis.upenn.edu/~boonloo/cis553-fa10/) Dr. Boon Thau Loo, Spring 2013, also Fall 2010
*   [Aalto University](https://noppa.tkk.fi/noppa/kurssi/s-38.2188/) Jose Costa-Requena and Markus Peuhkuri, Fall 2011
*   [Indian Institute of Technology Bombay](https://www.cse.iitb.ac.in/synerg/doku.php?id=public:courses:cs641-autumn08:start) Bhaskaran Raman, Autumn 2011, Autumn 2010, Autumn 2009, and Autumn 2008
*   University of Rijeka
    *   [RM2-Inf](https://lab.miletic.net/hr/nastava/kolegiji/RM2/), Dr. Mario Radovan and [Dr. Vedran Miletić](/wiki/User:Vedranm "User:Vedranm"), Spring 2015, Spring 2014, Spring 2013, and Spring 2012
    *   [RM-RiTeh](https://lab.miletic.net/hr/nastava/kolegiji/RM-RiTeh), Dr. Mladen Tomić and [Dr. Vedran Miletić](/wiki/User:Vedranm "User:Vedranm"), Spring 2014 and Spring 2013
*   [Queen's University CISC 834](http://research.cs.queensu.ca/~wireless_p/) Dr. Hossam S. Hassanein, Ramy Atawia and Hisham Farahat, Winter 2015, Fall 2013, and Fall 2012

## Other resources

*   Lalith Suresh's [Lab Assignments using ns-3](/wiki/Lab_Assignments_using_ns-3 "Lab Assignments using ns-3") page.

## ns-2 Education

This page contains references to the use of ns-2 and nam in education: [http://www.isi.edu/nsnam/ns/edu/](http://www.isi.edu/nsnam/ns/edu/)
