---
layout: page
title: Keynote
permalink: /research/wns3/wns3-2013/keynote/
---
### Olivier Dalle (INRIA): &#8220;Using Computer Simulations for Producing Scientific Results: Are We There Yet?&#8221;

**Abstract:** _&#8220;A rigourous scientific methodology has to follow a number of_ _supposedly well-known principles. These principles come from as far as_ _the ancient Greece where they started to be established by_ _Philosophers like Aristotle; later noticeable contributions include_ _principles edicted by Descartes, and more recently  Karl Popper. All_ _disciplines of modern Science do manage to comply with  those_ _principles with quite some rigor._

_All &#8230; Except maybe when they come to computer-based Science._ _Computer-based Science should not to be confused with the Computer_ _Science  discipline (a large part of which is not computer-based); It_ _designates the  corpus of scientific results obtained, in all_ _disciplines, by means of computers, using in-silico  experiments, and_ _in particular computer simulations. Issues and flaws in computer-based_ _Science started to be regularly pointed out in the scientific_ _community during the last decade._

_In this talk,  after a brief historical perspective, I will review_ _some of these major issues  and flaws, such as reproducibility of_ _results or reusability and traceability  of scientific software_ _material and data. Finally I will discuss a number of  ideas and_ _techniques that are currently investigated or could possibly  serve as_ _part of candidate solutions to solve those issues and flaws._

**Biography:** _&#8220;Olivier Dalle is Maître de Conférences in the Computer Sciences department of the Faculty of Sciences at the University of Nice-Sophia Antipolis (UNS).  He received his B.Sc. from the University of Bordeaux 1 and his M.Sc. and Ph.D.  from UNS.  From 1999 to 2000, he was a postdoctoral fellow at the French  Space Agency center in Toulouse (CNES-CST), where he started working  on component-based discrete-event simulation of multimedia telecommunication systems. Since 2000 he was member of several joint research  groups between UNS, CNRS, and INRIA, working on telecommunication  systems, distributed computing and component middle-ware. He has continuously been involved in the SIMUTools Conference since its beginning in 2008, successively as Program Chair, General Chair, and since 2009  as a member of the Steering Commitee. His current research interests  are discrete-event simulation (mainly methodology support), very large-scale networked systems, and component-based software engineering.&#8221;_
