---
layout: page
title: Program
permalink: /research/wns3/wns3-2013/program/
---
<table style="border: 1px solid black;" width="448" border="0" cellspacing="0">
  <colgroup span="3" width="85"></colgroup> <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
      9:00 &#8212; 10:00
    </td>

    <td style="border: 1px solid black;" align="LEFT">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Introduction
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Olivier Dalle. Using Computer Simulations for Producing Scientific Results: Are We There Yet?
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
      10:00 &#8212; 10:30
    </td>

    <td style="border: 1px solid black;" align="LEFT">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Coffee Break
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
      10:30 &#8212; 12:30
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Chair: Vedran Miletić
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Hajime Tazaki, Frederic Urbani and Thierry Turletti. DCE Cradle: Simulate Network Protocols with Real Stacks for Better Realism
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Brian Swenson and George Riley. Simulating Large Topologies in ns-3 using BRITE and CUDA Driven Global Routing
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Siddharth Gangadhar, Truc Anh Nguyen, Greeshma Umapathi and James Sterbenz. TCP Westwood Protocol Implementation in ns-3
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Peter Barnes, Betty Abelev, Eddy Banks, James Brase, David Jefferson, Sergei Nikolaev, Steven Smith and Ron Soltz. Integrating ns3 Model Construction, Description, Preprocessing, Execution, and Visualization
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
      12:00 &#8212; 13:30
    </td>

    <td style="border: 1px solid black;" align="LEFT">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Lunch
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
      13:30 &#8212; 15:30
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Chair: Luiz Felipe Perrone
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Yufei Cheng, Egemen Çetinkaya and James Sterbenz. Transactional Traffic Generator Implementation in ns-3
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Sebastien Deronne, Veronique Moeyaert and Sebastien Bette. Simulation of 802.11 Radio-over-Fiber Networks using ns-3
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Xiuchao Wu, Ken Brown, Cormac Sreenan, Pedro Alvarez, Marco Ruffini, Nicola Marchetti, David Payne and Linda Doyle. An XG-PON Module for the NS-3 Network Simulator
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
      15:30 &#8212; 17:00
    </td>

    <td style="border: 1px solid black;" align="LEFT">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Coffee Break and Poster/Demo Session
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
      16:30 &#8212; 18:00
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Chair: Tommaso Pecorella
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Giuseppe Piro, Luigi Alfredo Grieco, Gennaro Boggia and Pietro Camarda. Nano-Sim: simulating electromagnetic-based nanonetworks in the Network Simulator 3
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      Dizhi Zhou, Nicola Baldo and Marco Miozzo. Implementation and Validation for LTE Downlink Schedulers
    </td>
  </tr>

  <tr>
    <td style="border: 1px solid black;" align="LEFT" height="16">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
    </td>

    <td style="border: 1px solid black;" align="LEFT">
      David Gomez Fernandez, Ramón Agüero Calvo, Marta García-Arranz and Luis Muñoz Gutiérrez. Replication of the Bursty Behavior of Indoor WLAN Channels
    </td>
  </tr>
</table>
