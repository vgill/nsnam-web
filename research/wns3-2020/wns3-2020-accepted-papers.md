---
title: Accepted Papers
layout: page
permalink: /research/wns3/wns3-2020/accepted-papers/
---

  * **Sian Jin, Sumit Roy, Weihua Jiang, and Thomas Henderson,** An Efficient Abstraction Method for Implementing TGn Channel and OFDM-MIMO Error Models in ns-3

  * **Benjamin Silwa, Mahuel Patchou, Karsten Heimann, and Christien Wietfeld,** Simulating Hybrid Aerial- and Ground-based Vehicular Networks with ns-3 and LiMoSim

  * **Hao Yin, Pengyu Liu, Keshu Liu, Liu Cao, Lytianyang Zhang, Yayu Gao, and Xiaojun Hei,** NS3-AI:  Fostering Artificial Intelligence Algorithms for Networking Research

  * **Tommaso Zugno, Michele Polese, Natale Patriciello, Biljana Bojovic, Sandra Lagen, and Michele Zorzi,** Implementation of a Spatial Channel Model for ns-3

  * **Matteo Drago, Tommaso Zugno, Michele Polese, Marco Giordani, and Michele Zorzi,** MilliCar - An ns-3 Module for mmWave NR V2X Networks

  * **Vivek Jain, Thomas R. Henderson, Shravya K. S., and Mohit P. Tahiliani,** Data Center TCP in ns-3:  Implementation, Validation, and Evaluation

  * **Ahmet Kurt and Kemal Akkaya,** Connectivity Maintenance Extensions to IEEE 802.11s MAC Layer in NS-3

  * **Renato Cruz, Helder Fontes, José Ruela, Manuel Ricardo, and Rui Campos,** On the Reproduction of Real Wireless Channel Occupancy in ns-3

  * **Oscar Bautista Chia and Kemal Akkaya,** Extending IEEE 802.11s Mesh Routing for 3-D Mobile Drone Applications in NS-3

