---
layout: page
title: Accepted Papers
permalink: /research/wns3/wns3-2016/accepted-papers/
---
  * **Le Tian, S&eacute;bastien Deronne, Steven Latre and Jeroen Famaey**

    Implementation and validation of an IEEE 802.11ah module for NS-3
  * **Kurt Derr**

    NS-3 Web-Based User Interface &#8211; Power Grid Communications Planning and Modeling Tool
  * **Helder Fontes, Tiago Cardoso and Manuel Ricardo**

    Improving ns-3 Emulation Performance for Fast Prototyping of Network Protocols
  * **Andreas Lehmann, Matthias Kreuzer, J&ouml;rg Deutschmann, Ulrich Berold and Johannes Huber**

    Topology Simulation for Aeronautical Communication Protocols with ns-3 and DCE
  * **Patricia Deutsch, Leonid Veyster and Bow-Nan Cheng**

    LL SimpleWireless: A Controlled MAC/PHY Wireless Model to Enable Network Protocol Research
  * **Hany Assasa and Joerg Widmer**

    Implementation and Evaluation of a WLAN IEEE 802.11ad Model in NS-3
  * **Abdulhalim Dandoush, Alina Tuholukova, Sara Alouf, Giovanni Neglia, Sebastien Simoens, Pascal Derouet and Pierre Dersin**

    ns-3 based framework for simulating Communication Based Train Control (CBTC) systems
  * **Vishwesh Rege and Tommaso Pecorella**

    802.15.4 realistic MAC and Energy Model
  * **Russell Ford, Menglei Zhang, Sourjya Dutta, Marco Mezzavilla, Sundeep Rangan and Michele Zorzi**

    A Framework for Cross-Layer Evaluation of 5G mmWave Cellular Networks in ns-3
  * **Luciano Jerez Chaves, Islene Calciolari Garcia and Edmundo Roberto Mauro Madeira**

    OFSwitch13: Enhancing ns-3 with OpenFlow 1.3 support
  * **Pasquale Imputato and Stefano Avallone**

    Design and implementation of the traffic control module in ns-3
  * **Jared Ivey and George Riley**

    Analysis of Programming Language Overhead in DCE
  * **Paulo Regis, Suman Bhunia and Shamik Sengupta**

    Implementation of 3D Obstacle Compliant Mobility Models for UAV networks in ns-3
  * **Mohit Tahiliani, Shravya K. S and Smriti Murali**

    Implementation and Evaluation of Proportional Integral controller Enhanced (PIE) Algorithm in ns-3
  * **Truc Anh Nguyen, Siddharth Gangadhar, Md Moshfequr Rahman and James Sterbenz**

    An Implementation of Scalable, Vegas, Veno, and YeAH Congestion Control Algorithms in ns-3
  * **Hossein-Ali Safavi-Naeini, Farah Nadeem and Sumit Roy**

    Investigation and Improvements to the Physical Layer Abstraction for Wi-Fi in ns-3
  * **Mohit Tahiliani, Dharmendra Kumar Mishra and Pranav Vankar**

    TCP Evaluation Suite for ns-3
  * **Nestor Hernandez, Morten Pedersen, Peter Vingelmann, Janus Heide, Daniel Lucani and Frank Fitzek**

    Getting Kodo: Network Coding for the ns-3 simulator
