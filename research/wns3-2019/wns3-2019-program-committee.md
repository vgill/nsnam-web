---
layout: page
title: Program Committee
permalink: /research/wns3/wns3-2019/program-committee/
---

# General Chair

* Tommaso Pecorella, DINFO - UniFi, <tommaso.pecorella@unifi.it>

# Technical Program Co-Chairs

* Matthieu Coudron, IIJ-II, <coudron@iij.ad.jp>
* Damien Saucez, Inria, <damien.saucez@inria.fr>

# Proceedings Chair

* Eric Gamess, Jacksonville State University, <egamess@gmail.com>

# Technical Program Committee

Name                 | Institution                                        | 
---------------------|-----------------------------------------------------
Alexander Afanasyev  | University of California at Los Angeles, USA |
Ramon Aguero         | University of Cantabria, Spain |
Hany Assasa          | IMDEA Networks, Spain |
Peter Barnes         | Lawrence Livermore National Laboratory, USA |
Doug Blough          | Georgia Institute of Technology, USA |
Sébastien Deronne    | Televic Conference, Belgium |
Simone Ferlin        | Ericsson research, Sweden |
Helder Fontes        | University of Porto, Portugal |
Eric Gamess          | Jacksonville State University, USA |
Lorenza Giupponi     | Centre Tecnològic Telecomunicacions Catalunya, Spain |
Anil Jangam          | Cisco Systems, USA |
Sam Jansen           | StarLeaf Ltd., United Kingdom |
Dong Jin             | Illinois Institute of Technology, USA |
Leonardo Lanante     | Kyushu Institute of Technology, Japan |
Spyridon Mastorakis  | University of California at Los Angeles, USA |
Vedran Miletić       | University of Rijeka, Croatia |
Michele Polese       | University of Padova, Italy |
Sofie Pollin         | KU Leuven, Belgium |
Manuel Ricardo       | INESC TEC, Portugal      |
Mohit Tahiliani      | National Institute of Technology Karnataka, India |
Hajime Tazaki        | IIJ Innovation Institute, Japan |
Thierry Turletti     | Inria, France |
