---
layout: page
title: Accepted Papers
permalink: /research/wns3/wns3-2014/accepted-papers/
---
  * **Implementation and Evaluation of WAVE 1609.4/802.11p in ns-3**

    Junling Bu and Mingjian Liu.
  * **Extensions to LTE Mobility Functions for ns-3**

    Budiarto Herman, Nicola Baldo, Marco Miozzo, Manuel Requena and Jaime Ferragut.
  * **A TCP CUBIC Implementation in ns-3**

    Brett Levasseur, Mark Claypool and Robert Kinicki.
  * **Reliable Communications over Wireless Mesh Networks with Inter and Intra-Flow Network Coding**

    David Gomez Fernandez, Eduardo Rodríguez Maza, Ramon Aguero and Luis Munoz.
  * **Implementing Explicit Congestion Notification in ns-3**

    Brian Swenson and George Riley.
  * **Experiences Porting a Radio Model to NS-3 from OPNET Modeler**

    Alex White, Corwyn Miyagishima and Kenneth Renard.
  * **Simulation Speedup of NS3 Using Checkpoint and Restore**

    Kyle Harrigan and George Riley.
  * **Cyber-Physical Co-Simulation of Smart Grid Applications using ns-3**

    Muhammad Umer Tariq, Brian Paul Swenson, Arun Padmanabhan Narasimhan, Santiago Grijalva, George F. Riley and Marilyn Wolf.&nbsp;**Awarded Best Paper!**
