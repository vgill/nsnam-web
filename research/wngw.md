---
layout: page
title: Workshop on Next-Generation Wireless with ns-3
permalink: /research/wngw/
---

The **Workshop on Next-Generation Wireless (WNGW)** with ns-3 was a
specialized, single day workshop following WNS3 2019.  Extended abstracts are
published in the [ACM digital library](https://dl.acm.org/citation.cfm?id=3337941).

Future editions of this specialized workshop are to be determined.
