---
title: Videos
layout: page
permalink: /documentation/videos/
---
Several screencapture videos on installing and using ns-3 and [NetAnim](/wiki/NetAnim) are available:

  * The [ns-3 Consortium](/consortium/) has published some **[training videos](/consortium/activities/training/)** authored by ns-3 maintainers. 
  * John Abraham has several demonstration videos posted at [his ns3share page](http://www.youtube.com/user/ns3share/videos)
  * Reza Rezvani has prepared several [videos](http://www.youtube.com/channel/UCjfGsIMUbBY2DSCv-RKm1UQ/videos) on installing ns-3 and the [Eclipse IDE](http://www.eclipse.org) for OpenSUSE Linux
  * Tomé Gomes has prepared an [ns-3 tutorial](http://www.youtube.com/watch?v=AU7ZjBvQaBk)
  * Yodi Suhasta has prepared a short [ns-3 overview](http://www.youtube.com/watch?v=FFNvFzSmgIg) (in Indonesian)
  * Surya Penmetsa has a YouTube site with an [ns-3 tutorial playlist](http://www.youtube.com/playlist?list=PLmcMMZCV897qkb1fv177Y69hX8YhcmVpS)
